<?php
require_once('../load.php');
get_header();
is_driver();

$conn = db_conn();

$sql = 'SELECT username FROM driver WHERE username="'.$_SESSION['USER_USERNAME'].'" LIMIT 1';
$res = mysqli_query($conn, $sql);
if(mysqli_num_rows($res) > 0){
    redirect(ROOT_URL . '/driver');
} 
?>

<?php
$errors = [];
if(isset($_POST['submit'])){
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $national_id = $_POST['id'];
    $zone = $_POST['zone'];
    $birthday = $_POST['birthday'];
    $plate = $_POST['plate'];
    $color = $_POST['color'];
    $lat = $_POST['lat'];
    $long = $_POST['long'];
    $sql = 'INSERT INTO driver (username, first_name, last_name, car_color, car_tag, national_id, birthday, covered_zone, lng, lat) VALUES(
        "'.$_SESSION['USER_USERNAME'].'",
        "'.$fname.'",
        "'.$lname.'",
        "'.$color.'",
        "'.$plate.'",
        "'.$national_id.'",
        "'.$birthday.'",
        "'.$zone.'",
        "'.$long.'",
        "'.$lat.'"
    )';
    echo $sql;
    $res = mysqli_query($conn, $sql);
    if(!$res){
        $errors[] = "Error: ". mysqli_error($conn);
    }
    else redirect(ROOT_URL . '/driver');
}

?>
<div class="container">
    <div class="w-50 mx-auto card card-body mb-5">
        <h1 class="card-title">تکمیل ثبت نام</h1>
        <?php
        foreach($errors as $err){ ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <?=$err?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php }
        ?>
        <form method="POST" action="registration_complete.php">
            <div class="form-group">
                <label for="fname">نام :</label>
                <input type="text" class="form-control" name="fname" id="fname">
            </div>
            <div class="form-group">
                <label for="lname">نام خانوادگی :</label>
                <input type="text" class="form-control" name="lname" id="lname" >
            </div>
            <div class="form-group">
                <label for="id">* کد ملی : </label>
                <input type="number" class="form-control" name="id" id="id"  required>
            </div>
            <div class="form-group">
                <label for="zone">* ناحیه تحت پوشش : </label>
                <input type="text" class="form-control" name="zone" id="zone" required>
            </div>
            <div class="form-group">
                <label for="birthday">* تاریخ تولد :</label>
                <input type="text" class="form-control" name="birthday" id="birthday" required>
            </div>
            <div class="form-group">
                <label for="plate">* مدل ماشین :</label>
                <input type="text" class="form-control" name="plate" id="plate" required>
            </div>
            <div class="form-group">
                <label for="color">* رنگ ماشین :</label>
                <input type="text" class="form-control" name="color" id="color"  required>
            </div>
            <div class="form-group">
                <label for="lat">عرض:</label>
                <input type="number" class="form-control" name="lat" id="lat" >
            </div>
            <div class="form-group">
                <label for="long">طول :</label>
                <input type="number" class="form-control" name="long" id="long">
            </div>
            <button type="submit" name="submit" class="btn btn-primary btn-block">تائید</button>
        </form>
    </div>
</div>

<?php get_footer(); ?>