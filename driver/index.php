<?php
require_once('../load.php');
get_header();
$conn = db_conn();

is_driver();
$driver = get_driver();

if(isset($_POST['submit_status'])){
    $status = $_POST['status'];
    $zone = $_POST['zone'];
    $lat = $_POST['lat'];
    $long = $_POST['long'];

    $sql = 'UPDATE driver
            SET status="'.$status.'",
                covered_zone="'.$zone.'",
                lat="'.$lat.'",
                lng="'.$long.'"
            WHERE national_id = "'.$driver['national_id'].'"
        ';
    $res = mysqli_query($conn, $sql);
    if($res){
        $sql2 = 'INSERT INTO driver_trace (driver, status, zone, timestamp)
            VALUES(
                "'.$driver['national_id'].'",
                "'.$status.'",
                "'.$zone.'",
                "'.time().'"
            )';
        if($status != $driver['status'] OR $zone!=$driver['covered_zone'])
            mysqli_query($conn, $sql2);

        $submit_status_msg = "Status Updated.";
        $driver = get_driver();
    }
}

if(isset($_GET['delivered_id'])){
    $id = $_GET['delivered_id'];
    $sql = 'UPDATE send_request SET done=1 WHERE id="'.$id.'"';
    $res = mysqli_query($conn, $sql);
}

$sql = 'SELECT send_request.id, number, done, resturant.name as resturant_name, food.name as food, charity.name as charity_name
        FROM send_request
        INNER JOIN resturant ON send_request.resturant=resturant.username
        INNER JOIN charity ON send_request.charity=charity.username
        INNER JOIN food ON send_request.food=food.id
        WHERE 
            driver="'.$driver['national_id'].'" 
            AND done="0" 
            ORDER BY timestamp DESC 
            LIMIT 1
        ';
$res = mysqli_query($conn, $sql);
if(mysqli_num_rows($res) > 0){
    $request = mysqli_fetch_array($res);
    if($request['done'] == 1)
        unset($request);
}

?>

<div class="container">
    <div class="dashboard">
        <div class="row">
            <div class="col-3">
                <div class="sidebar">
                    <?php include_once('sidebar.php'); ?>
                </div>
            </div>
            <div class="col-9">
                <div class="mainbar">
                    <h2>
                       داشبورد راننده
                    </h2>
                    <hr/>
                    <?php
                    if(isset($submit_status_msg)){?>
                        <div class="alert alert-success">
                            <?=$submit_status_msg?>
                        </div>
                    <?php 
                    }
                    ?>
                    <h3>  تغییر وضعیت , مکان و منطقه : </h3>
                    <form class="mt-4" method="POST" action="index.php">
                        <div class="form-group row " >
                            <label class="col-2 col-form-label" for="status"> وضعیت :</label>
                            <div class="col-10">
                                <select class="form-control" name="status" id="status">
                                    <option value="available" <?=$driver['status']=='available' ? 'selected' : ''?>>دردسترس</option>
                                    <option value="unavailable" <?=$driver['status']=='unavailable' ? 'selected' : ''?>>خارج از دسترسی</option>
                                    <option value="busy" <?=$driver['status']=='busy' ? 'selected' : ''?> disabled>مشغول</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="zone"> منطقه :</label>
                            <div class="col-10">
                                <input type="text" class="form-control" id="zone" name="zone" placeholder="Enter zone" value="<?=$driver['covered_zone']?>" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group row d-flex justify-content-end">
                                    <label class="col-3 col-form-label" for="lat">Lat:</label>
                                    <div class="col-8">
                                        <input type="number" class="form-control" id="lat" name="lat" placeholder="Latitude" value="<?=$driver['lat']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group row d-flex justify-content-end" >
                                    <label class="col-3 col-form-label" for="long">Long:</label>
                                    <div class="col-8">
                                        <input type="number" class="form-control" id="long" name="long" placeholder="Longitude" value="<?=$driver['lng']?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <button type="submit" name="submit_status" class="btn btn-primary btn-block">به روز رسانی وضعیت</button>
                    </form>
                    <?php if(isset($request)):?>
                    <h3 class="mt-5">Current Request:</h3>
                    <table class="table">
                        <tbody>
                            <tr>
                                <td><b>From:</b></td>
                                <td><?=$request['resturant_name']?></td>
                            </tr>
                            <tr>
                                <td><b>To:</b></td>
                                <td><?=$request['charity_name']?></td>
                            </tr>
                            <tr>
                                <td><b>Number:</b></td>
                                <td><?=$request['number']?></td>
                            </tr>
                            <tr>
                                <td colspan="2"><a href="index.php?delivered_id=<?=$request['id']?>" class="btn btn-success btn-block text-white">Delivered</a></td>
                            </tr>
                        </tbody>
                    </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>