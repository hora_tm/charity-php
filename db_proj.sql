-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 28, 2020 at 03:34 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_proj`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_persian_ci NOT NULL,
  `zone` varchar(100) COLLATE utf8mb4_persian_ci NOT NULL,
  `street` varchar(100) COLLATE utf8mb4_persian_ci NOT NULL,
  `no` int(11) NOT NULL,
  `lat` decimal(50,0) NOT NULL,
  `lng` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `city`, `zone`, `street`, `no`, `lat`, `lng`) VALUES
(1, 'Mashhad', '', 'AhmadAbad', 12, '0', '0'),
(2, 'Mashhad', 'khayyam', '', 0, '0', '0'),
(3, 'Mashhad', 'khayyam', '', 0, '0', '0'),
(4, 'Mashhad', 'hashemie', '', 0, '0', '0'),
(5, 'Mashhad', 'kosar', '', 0, '0', '0'),
(6, 'Mashhad', '', '', 0, '0', '0'),
(7, 'Mashhad', '', '', 0, '0', '0'),
(8, 'Mashhad', '', '', 0, '0', '0'),
(9, 'Mashhad', 'khayyam', '', 0, '0', '0'),
(10, 'Mashhad', 'vakilabad', '', 0, '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `user` varchar(40) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`user`) VALUES
('alireza');

-- --------------------------------------------------------

--
-- Table structure for table `charity`
--

CREATE TABLE `charity` (
  `username` char(40) NOT NULL,
  `daily_food_count` int(11) DEFAULT NULL,
  `name` char(50) DEFAULT NULL,
  `address` int(150) DEFAULT NULL,
  `people_covered` int(11) DEFAULT NULL,
  `established_year` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `charity`
--

INSERT INTO `charity` (`username`, `daily_food_count`, `name`, `address`, `people_covered`, `established_year`) VALUES
('alireza', 98, 'Hazrat Zahra', 2, 100, 1398),
('hamid', 100, 'Hazrat Fateme', 3, 30, 1390),
('mamad', 106, 'Abshar Atefeha', 4, 200, 1392),
('mojtaba', 370, 'Golbarg', 5, 80, 1396);

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `username` char(40) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `car_color` char(20) DEFAULT NULL,
  `car_tag` char(20) DEFAULT NULL,
  `national_id` char(11) NOT NULL,
  `birthday` char(100) DEFAULT NULL,
  `status` char(30) DEFAULT 'unavailable',
  `covered_zone` char(60) DEFAULT NULL,
  `lng` decimal(10,0) DEFAULT NULL,
  `lat` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`username`, `first_name`, `last_name`, `car_color`, `car_tag`, `national_id`, `birthday`, `status`, `covered_zone`, `lng`, `lat`) VALUES
('mojtaba', 'Mojtaba', 'Shafiei', 'red', NULL, '0920726791', '1364/11/05', 'unavailable', 'vakilabad', '0', '0'),
('mostafa', 'Mostafa', 'Hooshyar', 'black', NULL, '0925671894', '1372/5/4', 'available', 'ghasemabad', '0', '0'),
('alireza', 'Alireza', 'Hooshyar', 'White', '12B744', '690726791', '8/12/1377', 'available', 'vakilabad', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `driver_trace`
--

CREATE TABLE `driver_trace` (
  `id` int(11) NOT NULL,
  `driver` char(11) CHARACTER SET utf8mb4 NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_persian_ci NOT NULL,
  `zone` varchar(100) COLLATE utf8mb4_persian_ci NOT NULL,
  `timestamp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `driver_trace`
--

INSERT INTO `driver_trace` (`id`, `driver`, `status`, `zone`, `timestamp`) VALUES
(12, '0920726791', 'available', 'vakilabad', 1580217003),
(13, '0920726791', 'unavailable', 'vakilabad', 1580217004),
(14, '0920726791', 'available', 'vakilabad', 1580217009),
(15, '0920726791', 'unavailable', 'vakilabad', 1580217011),
(16, '0920726791', 'available', 'vakilabad', 1580217013),
(17, '0920726791', 'unavailable', 'vakilabad', 1580217015),
(18, '0925671894', 'available', 'ghasemabad', 1580217027);

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE `food` (
  `id` int(11) NOT NULL,
  `name` char(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`id`, `name`) VALUES
(2, 'Bacon'),
(3, 'Bagel toast'),
(4, 'Baked bean'),
(5, 'Barbecue'),
(6, 'Bauru'),
(7, 'Bologna'),
(8, 'Bratwurst'),
(9, 'Breakfast'),
(10, 'Bun kebab'),
(11, 'Butterbrot'),
(12, 'Chacarero'),
(13, 'Cokodok'),
(14, 'Khanom buang'),
(15, 'Pretzel'),
(16, 'Waffle'),
(17, 'Candy');

-- --------------------------------------------------------

--
-- Table structure for table `resturant`
--

CREATE TABLE `resturant` (
  `username` char(40) NOT NULL,
  `name` char(100) DEFAULT NULL,
  `address` int(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `resturant`
--

INSERT INTO `resturant` (`username`, `name`, `address`) VALUES
('ali', 'Asghar Mashti', 6),
('alireza', 'Resturan Khafan', 9),
('kourosh', 'Fati o pesaran', 10),
('mostafa', 'Awli Kabab', 7),
('shahram', 'Resturan Shabpare', 8);

-- --------------------------------------------------------

--
-- Table structure for table `resturant_charity`
--

CREATE TABLE `resturant_charity` (
  `resturant` char(40) CHARACTER SET utf8mb4 NOT NULL,
  `charity` char(40) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `resturant_charity`
--

INSERT INTO `resturant_charity` (`resturant`, `charity`) VALUES
('alireza', 'alireza'),
('alireza', 'hamid'),
('alireza', 'mojtaba'),
('kourosh', 'hamid'),
('kourosh', 'mojtaba');

-- --------------------------------------------------------

--
-- Table structure for table `resturant_has_food`
--

CREATE TABLE `resturant_has_food` (
  `resturant` char(40) CHARACTER SET utf8mb4 NOT NULL,
  `food_id` int(11) NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `secrity_question`
--

CREATE TABLE `secrity_question` (
  `id` int(11) NOT NULL,
  `question` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `secrity_question`
--

INSERT INTO `secrity_question` (`id`, `question`) VALUES
(1, 'What is the name of your first teacher?'),
(2, 'Wich city did you born?');

-- --------------------------------------------------------

--
-- Table structure for table `send_request`
--

CREATE TABLE `send_request` (
  `id` int(11) NOT NULL,
  `resturant` char(40) CHARACTER SET utf8mb4 NOT NULL,
  `charity` char(40) CHARACTER SET utf8mb4 NOT NULL,
  `driver` char(11) CHARACTER SET utf8mb4 DEFAULT NULL,
  `food` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `rate` int(11) DEFAULT NULL,
  `done` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `send_request`
--

INSERT INTO `send_request` (`id`, `resturant`, `charity`, `driver`, `food`, `number`, `timestamp`, `rate`, `done`) VALUES
(31, 'alireza', 'mamad', '690726791', 7, 10, 1580140583, 4, 1),
(32, 'alireza', 'mamad', '690726791', 6, 10, 1580140752, 5, 1),
(33, 'alireza', 'mamad', '0920726791', 10, 10, 1580140758, 4, 1),
(34, 'alireza', 'hamid', '0920726791', 11, 10, 1580140791, 1, 1),
(35, 'alireza', 'hamid', '0925671894', 11, 10, 1580140826, 3, 1),
(36, 'alireza', 'hamid', '0920726791', 11, 10, 1580140847, 2, 1),
(37, 'alireza', 'hamid', '0925671894', 11, 10, 1580140882, 4, 1),
(38, 'kourosh', 'mojtaba', '0925671894', 10, 10, 1580198640, 5, 1),
(39, 'kourosh', 'hamid', '0925671894', 13, 40, 1580198671, 5, 1),
(40, 'kourosh', 'mojtaba', '690726791', 6, 10, 1580198724, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` char(30) NOT NULL,
  `password` char(255) NOT NULL,
  `role` varchar(20) DEFAULT NULL,
  `question_id` int(11) NOT NULL,
  `question_answer` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `role`, `question_id`, `question_answer`) VALUES
('ali', '$2y$10$qzISnokErAMhXCxxi2PVyOpVtWOGK728GxHMrlnxrTtn13b.xCifq', 'resturant', 1, 'mamad'),
('alireza', '$2y$10$yEaJfee4zSMwEeqz6ynzyOKMpcFNC8bzQFodG7MTrPsYGTqsZzo8a', 'driver', 1, 'Mamad'),
('hamid', '$2y$10$PYCT28RhCoavFtt7rC2iYOv5QLjujSCugwMKO8RY908cLePp3drxa', 'charity', 1, 'mamad'),
('kourosh', '$2y$10$U40E9AElONO9JenXawe9TOER/XlQuewgPu47vk9yAVjKY9A2crsU2', 'resturant', 2, 'mashhad'),
('mamad', '$2y$10$508ktut.poQ9gHcI/70wh.bqqoWZ38l9Caa4kQo0qqfRKqsEP3qoG', 'charity', 1, 'mamad'),
('mojtaba', '$2y$10$qqPbxV6Er3edSx2a.jZRp.0eqlWwmAbOsQAnYfbBv1aNSBryd0Jyq', 'driver', 1, 'mamad'),
('mostafa', '$2y$10$hsPD9TFCZsZM/lpyJo2GVekaiT4LuuHTbtye7Gl0MVyZNCENbbPfS', 'driver', 1, 'mamad'),
('shahram', '$2y$10$hAgBsfuSQfD58O4JgrxGXuiMEKCEjxkcOKFe6MmP.jz.QkoZqeaX.', 'resturant', 1, 'mamad');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`user`);

--
-- Indexes for table `charity`
--
ALTER TABLE `charity`
  ADD PRIMARY KEY (`username`),
  ADD KEY `username` (`username`),
  ADD KEY `address` (`address`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`national_id`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `driver_trace`
--
ALTER TABLE `driver_trace`
  ADD PRIMARY KEY (`id`),
  ADD KEY `driver` (`driver`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resturant`
--
ALTER TABLE `resturant`
  ADD PRIMARY KEY (`username`),
  ADD KEY `address` (`address`);

--
-- Indexes for table `resturant_charity`
--
ALTER TABLE `resturant_charity`
  ADD PRIMARY KEY (`resturant`,`charity`),
  ADD KEY `charity` (`charity`);

--
-- Indexes for table `resturant_has_food`
--
ALTER TABLE `resturant_has_food`
  ADD KEY `food_id` (`food_id`),
  ADD KEY `resturant` (`resturant`);

--
-- Indexes for table `secrity_question`
--
ALTER TABLE `secrity_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `send_request`
--
ALTER TABLE `send_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `charity` (`charity`),
  ADD KEY `resturant` (`resturant`),
  ADD KEY `driver` (`driver`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`),
  ADD KEY `question_id` (`question_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `driver_trace`
--
ALTER TABLE `driver_trace`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `secrity_question`
--
ALTER TABLE `secrity_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `send_request`
--
ALTER TABLE `send_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`username`);

--
-- Constraints for table `charity`
--
ALTER TABLE `charity`
  ADD CONSTRAINT `charity_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`),
  ADD CONSTRAINT `charity_ibfk_2` FOREIGN KEY (`address`) REFERENCES `address` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `driver`
--
ALTER TABLE `driver`
  ADD CONSTRAINT `driver_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Constraints for table `driver_trace`
--
ALTER TABLE `driver_trace`
  ADD CONSTRAINT `driver_trace_ibfk_1` FOREIGN KEY (`driver`) REFERENCES `driver` (`national_id`);

--
-- Constraints for table `resturant`
--
ALTER TABLE `resturant`
  ADD CONSTRAINT `resturant_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`),
  ADD CONSTRAINT `resturant_ibfk_2` FOREIGN KEY (`address`) REFERENCES `address` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `resturant_charity`
--
ALTER TABLE `resturant_charity`
  ADD CONSTRAINT `resturant_charity_ibfk_1` FOREIGN KEY (`charity`) REFERENCES `charity` (`username`),
  ADD CONSTRAINT `resturant_charity_ibfk_2` FOREIGN KEY (`resturant`) REFERENCES `resturant` (`username`);

--
-- Constraints for table `resturant_has_food`
--
ALTER TABLE `resturant_has_food`
  ADD CONSTRAINT `resturant_has_food_ibfk_1` FOREIGN KEY (`food_id`) REFERENCES `food` (`id`),
  ADD CONSTRAINT `resturant_has_food_ibfk_2` FOREIGN KEY (`resturant`) REFERENCES `resturant` (`username`);

--
-- Constraints for table `send_request`
--
ALTER TABLE `send_request`
  ADD CONSTRAINT `send_request_ibfk_1` FOREIGN KEY (`charity`) REFERENCES `charity` (`username`),
  ADD CONSTRAINT `send_request_ibfk_2` FOREIGN KEY (`resturant`) REFERENCES `resturant` (`username`),
  ADD CONSTRAINT `send_request_ibfk_3` FOREIGN KEY (`driver`) REFERENCES `driver` (`national_id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `secrity_question` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
