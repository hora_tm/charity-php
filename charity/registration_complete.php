<?php
require_once('../load.php');
get_header();
is_charity();

$conn = db_conn();

$sql = 'SELECT username FROM charity WHERE username="'.$_SESSION['USER_USERNAME'].'" LIMIT 1';
$res = mysqli_query($conn, $sql);
if(mysqli_num_rows($res) > 0){
    redirect(ROOT_URL . '/charity');
} 
?>

<?php
$errors = [];
if(isset($_POST['submit'])){
    $name = $_POST['name'];
    $year = $_POST['year'];
    $number = $_POST['number'];

    $city = $_POST['city'];
    $zone = $_POST['zone'];
    $street = $_POST['street'];
    $no = $_POST['no'];
    $lat = $_POST['lat'];
    $long = $_POST['long'];
    
    $sql = 'INSERT INTO address (city, zone, street, no, lat, lng)
        VALUES ("'.$city.'", "'.$zone.'", "'.$street.'", "'.$no.'", "'.$lat.'", "'.$long.'")
    ';
    $res = mysqli_query($conn, $sql);
    if(!$res){
        $errors[] = "Error: ". mysqli_error($conn);
    }
    else{
        $address_id = mysqli_insert_id($conn);
        $sql = 'INSERT INTO charity (username, daily_food_count, name, address, people_covered, established_year) VALUES(
            "'.$_SESSION['USER_USERNAME'].'",
            "'.$number.'",
            "'.$name.'",
            "'.$address_id.'",
            "'.$number.'",
            "'.$year.'"
        )';
        $res = mysqli_query($conn, $sql);
        if(!$res){
            $errors[] = "Error: ". mysqli_error($conn);
        }
        else redirect(ROOT_URL . '/charity');
    }
}

?>
<div class="container">
    <div class="w-50 mx-auto card card-body">
        <h1 class="card-title">تکمیل ثبت نام</h1>
        <?php
        foreach($errors as $err){ ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <?=$err?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php }
        ?>
        <form method="POST" action="registration_complete.php">
            <div class="form-group">
                <label for="name"> : نام خیریه  * </label>
                <input type="text" class="form-control" name="name" id="name" placeholder="نام خیریه را وارد کنید" required>
            </div>
            <div class="form-group">
                <label for="year">: سال تاسیس  *</label>
                <input type="number" class="form-control" name="year" id="year" placeholder="سال تاسیس را وارد کنید" required>
            </div>
            <div class="form-group">
                <label for="number">: تعداد افراد تحت پوشش *</label>
                <input type="number" class="form-control" name="number" id="number" placeholder="تعداد افراد تحت پوشش را وارد کنید" required>
            </div>
            <h3>آدرس</h3>
            <div class="form-group">
                <label for="city">: شهر  *</label>
                <input type="text" class="form-control" name="city" id="name" placeholder="شهر را وارد کنید" required>
            </div>
            <div class="form-group">
                <label for="zone"> : منطقه</label>
                <input type="text" class="form-control" name="zone" id="zone" placeholder="منطقه را وارد کنید">
            </div>
            <div class="form-group">
                <label for="st"> : خیابان</label>
                <input type="text" class="form-control" name="street" id="st" placeholder="خیابان را وارد کنید">
            </div>
            <div class="form-group">
                <label for="no"> : پلاک </label>
                <input type="text" class="form-control" name="no" id="no" placeholder="پلاک را وارد کنید">
            </div>
            <div class="form-group">
                <label for="lat">Lat:</label>
                <input type="number" class="form-control" name="lat" id="lat" placeholder="Enter Lat">
            </div>
            <div class="form-group">
                <label for="long">Long:</label>
                <input type="number" class="form-control" name="long" id="long" placeholder="Enter Long">
            </div>
            <button type="submit" name="submit" class="btn btn-primary btn-block">تکمیل ثبت نام</button>
        </form>
    </div>
</div>

<?php get_footer(); ?>