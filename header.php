<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>پروژه  خیریه</title>
    <link rel="stylesheet" href="<?=STATIC_ROOT ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=STATIC_ROOT ?>/css/styles.css">

  
</head>

<body>


    <nav class="w-100 bg-white px-5 mb-5" style="border-radius:5px; box-shadow: 0 4px 2px -2px rgba(0,0,0,.2);">
                <form method="post" style="display:flex;align-items:center">
                <select id="main_language" name ="main_language" class="btn btn-primary py-2 px-2 my-1">
                    <option value="EN">EN</option>
                    <option value="FA">FA</option>
                    <!-- <option value="third">Third</option> -->
                </select>
                <input type="submit" value="Submit" style="background-color:gray;border-radius:5px;outline:none;border:none;" class="ml-2 p-2"/>
                </form>
        </nav>


<?php
    static $ui_lan;
      $ui_lan = isset($_POST['main_language']) ? $_POST['main_language'] : false;
    //   echo   $ui_lan ;
    if($ui_lan == "EN") {
        $textAlign = "left";
        $direction = "ltr"; 
        }
        else {
        $textAlign = "right";
        $direction = "rtl";  
      } 
?>
<style>
    * {
        /* display:flex; */
        text-align : <?php echo $textAlign; ?>;
       direction : <?php echo $direction; ?>;
    }

    #role {
        direction : <?php echo $direction; ?>;
    }
    </style>